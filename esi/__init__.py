"""Django app for accessing the EVE Swagger Interface (ESI)."""

default_app_config = 'esi.apps.EsiConfig'

__version__ = '5.2.0'
__title__ = 'django-esi'
